function fish_right_prompt
    set prev_status $status
    echo -n (set_color yellow)$prev_status
end

function fish_prompt
    set __git_status (git status 2> /dev/null | head -1)

    echo -n -e "\n"(set_color red --bold)"$USER@$(hostname) "
	echo -n (set_color blue --bold)(prompt_pwd)

    if [ $__git_status!="" ]
        string match -q "On branch *" "$__git_status"; 
          and string replace "On branch " "" "$__git_status" | read -l __git_status;
          and set_color b8bb26 --bold;
        
        echo -n " ($__git_status)"

	end

	echo -n -e "\n"(set_color normal)"#> "
end

#set LANG "en_US.UTF-8"
#set LANGUAGE "en_US.UTF-8"
set BROWSER firefox
set EDITOR nvim

#alias cat bat
#alias db distrobox
alias weather "curl wttr.in/Kiryat+Bialik"
alias v vim
alias bt bluetoothctl
alias yt-download "yt-dlp -f bestvideo+bestaudio"

if status is-interactive
	clear && neofetch
end
