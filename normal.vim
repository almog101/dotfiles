set termguicolors
set clipboard+=unnamedplus
set showtabline=2
set nocompatible
set visualbell
set encoding=UTF-8
filetype on
set smarttab
set path+=**
filetype plugin on
filetype indent on
syntax on
set number relativenumber
set mouse=a
set shiftwidth=4
set tabstop=4
autocmd Filetype html setlocal tabstop=2 shiftwidth=2 expandtab
set nobackup
set nowrap
set incsearch
set nohlsearch
set ignorecase
set showcmd
set showmode
set showmatch
set history=1000
set splitbelow splitright
set wildmenu
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
set colorcolumn=80
set signcolumn
set cursorline

if version >= 703
	set undodir=~/.vim/backup
	set undofile
	set undoreload=10000
endif

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_altv = 1
let g:netrw_winsize = 25
autocmd FileType netrw autocmd BufLeave <buffer> if &filetype == 'netrw' | :bd | endif

" Minimalist-Tab Complete
inoremap <expr> <Tab> TabComplete()
fun! TabComplete()
	if getline('.')[col('.') - 2] =~ '\K' || pumvisible()
		return "\<C-N>"
	else
		return "\<Tab>"
	endif
endfun

" Closing compaction in insert mode
	"inoremap [ []<left>
	"inoremap ( ()<left>
	"inoremap { {}<left>
	"inoremap /* /**/<left><left>

"------------------KEY_BINDINGS------------------

" Map Alt-, to move to the previous tab
	nnoremap <M-,> gT

" Map Alt-. to move to the next tab
	nnoremap <M-.> gt

" Set the space  as the leader key.
	let mapleader = " "


" Opening\Creating a file in a new tab - write the tab to open
	nnoremap <leader>c :tabedit<CR>

" bind tab to auto complete
	inoremap <Tab> <C-n>
	inoremap <S-Tab> <C-p>

" wildmenu bindings
cnoremap <Right> <Space><BS><Right><C-z>
cnoremap <expr> <Up> pumvisible() ? "\<C-p>" : "\<Up>"
cnoremap <expr> <Down> pumvisible() ? "\<C-n>" : "\<Down>"


"------------------END_KEY------------------

"------------------Cursor Shape------------------
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

set ttimeout 
set ttimeoutlen=1 
set ttyfast

" Show tabs
autocmd VimEnter * tab all 
